---
title: 'Email: Alias einrichten'
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - alias
        - settings
        - einstellungen
        - einrichten
page-toc:
    active: false
---

# Email-Aliase

Nachdem Du mit diesem [Formular](https://disroot.org/de/forms/alias-request-form) Aliase beantragt (und genehmigt bekommen) hast, musst Du sie noch einrichten. In diesem Bereich erklären wir Dir, wie Du das bei verschiedenen Email-Clients tun kannst.

- [Webmail](webmail)
- [Thunderbird](thunderbird)
- [K-9 Mail](k9)
- [FairEmail](fairemail)
- [Mail iOS](mailios)
