---
title: Filtres de courriel
published: true
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - filters
        - settings
page-toc:
    active: false
---

# Filtres de courriel

Les filtres de courriels vous permettent de gérer les courriels entrants de façon automatisée, tels que le déplacement du courrier entrant vers un répertoire en fonction de certains critères, la configuration de réponse automatique "en dehors du bureau" ou "vacances", le rejet ou le transfert automatique des courriels, etc.

Dans cette section, nous aborderons les principes de base basés sur certains scénarios.
