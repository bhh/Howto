---
title: Red social
subtitle: "DisHub: Configuraciones, lo básico, permisos"
icon: fa-asterisk
published: true
taxonomy:
    category:
        - docs
        - topic
    tags:
        - dishub
        - nomad
        - hubzilla
page-toc:
    active: false
---

# DisHub: la instancia Hubzilla de Disroot

Introducción a **Dishub/Hubzilla** y **Nomad**, la aplicación para móviles de **DisHub**.
<br>
<br>
