---
title: Hubzilla
updated:
published: true
visible: true
indexed: true
taxonomy:
    category:
        - docs
    tags:
        - hubzilla
        - dishub
page-toc:
    active: false
---

![](en/hubzilla_disroot.png)

# DisHub: Disroot's Hubzilla instance

[**Hubzilla**](https://project.hubzilla.org/page/hubzilla/hubzilla-project) is a powerful, free and open source software to create decentralized and federated web platforms which includes webpages tools, wikis, forums, cloud storage, social networks and more. Any attempt to accurately define Hubzilla may end up being inadequate.<br>
While it shares similarity with other web services software, **Hubzilla** offers some very particular features that no others do, like full and precise permission system that gives you control about the privacy of the information and content you may share; nomadic identity (which means you own your online identity and can take/migrate it across the network, no matter the hubs, instance, servers); multiple profiles and channels; account cloning; among other features.

At **Disroot**, we think it’s an awesome piece of software and that everybody should try it because of its potential and the power it’s giving to decentralization and federation services. So we built **DisHub**, the **Disroot’s Hubzilla instance**. But, due to the amount of features, apps, services and user control settings, and because Hubzilla is beyond comparison, it may be kind of difficult until you fully get it. That’s why we made this Howto.

Think of Hubzilla as your new digital house: You have many rooms that you can arrange and decorate as you want, it’s your place to talk and share whatever you want with the people you want. And more than that, because you can even take your home wherever (other Hubzilla instances) you want.

So, exercise your freedom, your imagination and welcome to DisHub.

*We can only show you the door. You're the one that has to walk through it...*

### Basics
  - [How to start really quick?](basics)
  - [User interface overview](basics/user_interface)
  - [Navigation bar](basics/navigation_bar)

### Channels
  - [What is a channel?](channels)
  - [Creating a channel](channels/creation)
  - [Profiles](channels/profiles)
  - [Blocking a channel](channels/blocking)
  - [Ignoring a channel](channels/ignoring)
  - [Archiving a channel](channels/archiving)
  - [Hiding a channel](channels/hiding)

### [Connections](connections)

### [Permissions](permissions)
  - [Default channel permissions limits](permissions/default_channel_permissions-limits)
  - [Channel permissions roles](permissions/channel_permission_roles)
  - [Channel permissions limits](permissions/channel_permission_limits)
  - [ACL: Access Control List](permissions/acl)
  - [Guest Access Token](permissions/guest_access_tokens)
  - [Channel roles permissions table](permissions/channel_roles)

### [Posting and publishing](posting)
  - [Composing](posting/composing)
  - [Tags and mentions](posting/tags_and_mentions)
  - Published post options (Coming Soon)

### [Wiki](wiki)

### [Features](features)
  - [Connections Filtering](features/connection_filtering)
  - [Personal Cloud Storage](features/personal_cloud_storage)
  - [Bookmarks](features/bookmarks)
  - [Chat](features/chat)
  - [Calendar](features/calendar)
  - [Photo album](features/photo_album)

### [FAQ](faq)
