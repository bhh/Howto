---
title: 'Email alias request'
published: true
indexed: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - user
        - account
        - alias
page-toc:
    active: false
---

# How to request email aliases
The email aliases feature is offered as a "reward" for regular supporters. By regular supporters we think of those who contribute with, at least, the equivalent of one cup of coffee a month. In order for the 'reward' not becoming the only incentive to donate, we expect to see the donation made before we can enable this feature.

It is not that we are promoting coffee (which is, actually, a very handy symbol for [exploitation and inequality](http://www.foodispower.org/coffee/)). We thought that is a good way to let people measure themselves how much they can give.

**Please take time to consider your contribution.** If you can 'buy' us one cup of **Rio De Janeiro** coffee a month that's OK, but if you can afford a *Double Decaf Soy Frappuccino With An Extra Shot And Cream* a month, then you can really help us keep the **Disroot** platform running and make sure it is available for free for other people with less means.

We found this [list](https://www.caffesociety.co.uk/blog/the-cheapest-cities-in-the-world-for-a-cup-of-coffee) of coffee cup prices around the world, it might not be very accurate, but it gives a good indication of the different fares.

So, **to request aliases** you need to fill in this [form](https://disroot.org/forms/alias-request-form).

Now, if you're looking for **how to setup an email alias**, then take a look at [this tutorial](/tutorials/email/alias)
