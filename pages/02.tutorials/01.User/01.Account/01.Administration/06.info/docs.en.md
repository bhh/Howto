---
title: 'Account information'
published: true
indexed: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - user
        - account
        - information
page-toc:
    active: true
---

# Your Account information

![](en/dashboard_info.png)

Here you can find a summary of the **account and password information**, **the password policy** (the rules that a password must comply with) and the **history** of the password management.

![](en/account.png)

This section is merely informative.
