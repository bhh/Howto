---
title: "Discourse: Esporta i tuoi post pubblicati sul forum"
published: true
indexed: true
updated:
    last_modified: "July 2019"		
    app: Discourse
    app_version: 2.3
taxonomy:
    category:
        - docs
    tags:
        - user
        - forum
        - discourse
        - gdpr
visible: true
page-toc:
    active: false
---

**Discourse**, il software utilizzato da **Disroot** per il **Forum**, ti consente di esportare il contenuto del testo da tutti i tuoi post in un file .csv (che è supportato dalla maggior parte dei fogli di calcolo (Libreoffice, Openoffice, Gnumeric, Excel).

**Per esportare i tuoi post da Discourse:**
- Clicca sul tuo avatar in alto a destra
![](en/export_data_discourse_01.png)<br>
- Clicca sul tuo yousername
![](en/export_data_discourse_02.png)<br>
- Clicca su _"Scarica i Miei Messaggi"_
![](en/export_data_discourse_03.png)<br>
- Clicca su _"Sì"_ e successivamene quando la finestra di pop-up ti chiederà se vuoi scaricare i tuoi post, premi su  _"Ok"_.

**ATTENZIONE:** I dati possono essere scaricati solo una volta ogni 24 ore.

![](en/export_data_discourse_01.gif)

          
Riceverai un messaggio dal sistema che ti informerà che i dati sono pronti per essere scaricati e ti fornirà un link per scaricare il file .csv con una copia dei tuoi post.
Se hai abilitato le notifiche via e-mail, riceverai anche un'email con queste informazioni. Basta premere il link per scaricare il file.

I dati saranno compressi come un archivio gzip. Se l'archivio non si estrae quando lo si apre, utilizzare gli strumenti consigliati qui: http://www.gzip.org/#faq4

Il collegamento sarà disponibile per 48 ore, dopodiché scadrà e dovrai esportare nuovamente i tuoi dati.

Quando avrai estratto il file, potrai aprirlo con un software per i fogli di calcolo.
