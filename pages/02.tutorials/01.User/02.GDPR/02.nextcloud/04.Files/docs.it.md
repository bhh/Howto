---
title: Nextcloud: File e Note
published: true
indexed: true
updated:
    last_modified: "July 2019"		
    app: Nextcloud
    app_version: 15
taxonomy:
    category:
        - docs
    tags:
        - user
        - cloud
        - files
        - notes
        - gdpr
visible: true
page-toc:
    active: false
---

Puoi scaricare i tuoi file i n modo molto semplice su **Nextcloud**:app.

1. Accedi al [cloud](https://cloud.disroot.org)

2. Seleziona l'applicazione **File**

3. Seleziona tutti i file cliccando sul quadratino (checkbox)

4. Successivamente clicca su menu **Azioni** e scegli *Download*

![](en/files_app.gif)
