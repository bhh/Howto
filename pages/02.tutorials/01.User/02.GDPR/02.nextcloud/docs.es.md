---
title: RGDP: Nextcloud
published: true
indexed: false
taxonomy:
    category:
        - docs
    tags:
        - user
        - rgdp
visible: true
page-toc:
    active: false
---
## Cómo exportar tus...

  - [**Archivos & Notas**](files)
  - [**Contactos**](contacts)
  - [**Calendarios**](calendar)
  - [**Noticias**](news)
  - [**Marcadores**](bookmarks)
