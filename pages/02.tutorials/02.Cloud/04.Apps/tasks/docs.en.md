---
title: Tasks
published: true
visible: false
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - task
        - sync
    visible: true
page-toc:
    active: false
---

# Tasks
**Tasks** app allows you to add, edit and delete tasks in your **Cloud**. They can be shared between users, synchronized using [**CalDav**](https://en.wikipedia.org/wiki/CalDAV) to sync them to your local client and even downloaded as [ICS](https://en.wikipedia.org/wiki/ICalendar) files.

---

## [Web interface](web)
- Creating and configuring tasks

## [Desktop clients](desktop)
- Desktop clients and applications for organizing and synchronizing tasks

## [Mobile clients](mobile)
- Mobile clients and settings for organizing and synchronizing tasks
