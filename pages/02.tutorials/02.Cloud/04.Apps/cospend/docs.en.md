---
title: Cospend
published: true
visible: false
indexed: true
updated:
        last_modified: " 2021"
        app: Cospend
        app_version: 1.2.7
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - apps
        - cospend
visible: true
page-toc:
    active: false
---

# Cospend (coming soon)

**Cospend** app is a group/shared budget manager. You can use it when you share a house, when you go on vacation with friends or whenever you share money with others.
