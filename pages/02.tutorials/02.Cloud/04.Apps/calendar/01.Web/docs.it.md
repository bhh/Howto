---
title: "Calendario: Web"
published: true
indexed: true
visible: false
updated:
        last_modified: "July 2019"
        app: Nextcloud
        app_version: 15
taxonomy:
    category:
        - docs
    tags:
        - calendar
        - cloud
page-toc:
    active: true
---

# Utilizzare l'applicazione calendario attraverso l'interfaccia web

Puoi accedere all'applicazione del tuo calendario premendo l'icona ![](en/calendar_top_icon.png) {.inline} nella barra in alto.
*Barra sinistra*nella finestra del calendario offre una panoramica dei calendari, delle loro opzioni e impostazioni generali.. 

![](en/calendar_main.png)


## Creare calendari
Per creare un nuovo calendario in questo pannello opzioni premere "*crea calendario*"
Verrà visualizzato un piccolo prompt in cui è possibile digitare il nome che si desidera assegnare a questo nuovo calendario e selezionare un colore per esso.
Quindi premere "*crea*".

![](en/calendar_add_new.png)

Puoi creare più calendari per scopi diversi (lavoro, attivismo, ecc.) ripetendo questo processo. Se lo fai, puoi usare colori diversi per distinguerli (l'ultima icona è il selettore colori che ti consente di scegliere qualsiasi colore tu preferisca![](en/calendar_colorpick_icon.png)).

![](en/calendar_list.png)


## Elimina, modifica e scarica il calendario.
Sul pannello di sinistra, vedrai i tuoi calendari elencati. A destra di ogni calendario troverai un pulsante "altro" dove puoi:
- rinominare il tuo calendario
- scaricarlo
- accedere all'indirizzo per la sincronizzazione con gli altri dispositivi
- eliminare il tuo calendario

![](en/calendar_edit1.png)


## Creare un evento
Puoi creare un nuovo evento nel tuo calendario facendo clic, nella pagina principale del calendario, sul giorno dell'evento. Apparirà un pannello sul lato destro, dove puoi riempire con le informazioni dell'evento.

![](en/calendar_edit_menu.png)

Nello specifico in questo pannelo vedrai:

  - titolo dell'evento
  - data di inizio e di fine
  - orario di inizio e di fine
  - se è un evento che dura tutta la giornata o meno
  - luogo dell'evento
  - descrizione dell'evento

![](en/calendar_edit_menu2.png)

Se hai più calendari, nell'app Disroot Calendar, devi selezionare su quale calendario dovrà andare l'evento. Puoi farlo sotto il campo del titolo dell'evento.

![](en/calendar_edit_menu3.png)

Puoi impostare un promemoria per l'evento premendo "Promemoria" e "Aggiungi".

![](en/calendar_edit_menu4.png)

Puoi decidre il tipo di promemoria desiderato:

* audio
* email
* pop up

E settarlo per l'evento desiderato.

Basta premere il promemoria che hai aggiunto e le opzioni verranno visualizzate.

![](en/calendar_edit_menu5.png)

Puoi anche impostare se si tratta di un evento ricorrente o meno. Controlla le opzioni * ripetute *.

![](en/calendar_edit_menu6.png)


## Invitare persone all'evento

Poi anche invitare persone all'evento via email:

* premi su "Partecipanti"
* compila il campo con il nome della persona o l'indirizzo di posta elettronica
* premi enter

![](en/calendar_edit_menu7.png)

Le persone che inviti riceveranno un'e-mail generata automaticamente con l'invito. Eventuali modifiche apportate all'evento verranno inviate automaticamente via e-mail alla persona che hai aggiunto.

Quando hai finito con le modifiche, basta premere crea alla fine del pannello e il tuo evento verrà visualizzato sullo schermo.


## Editare o eliminare eventi
Per modificare o eliminare un evento che hai creato, fai clic sull'evento sullo schermo, modificalo e quindi premi "aggiorna".
Per eliminarlo, troverai il grande pulsante rosso.

![](en/calendar_edit_menu8.png)


## Condividere calendari
Puoi condividere i tuoi calendari, con un altro utente, tramite e-mail o link pubblico.
Per condividere un calendario con un altro utente di Disroot:

* premi il pulsante di condivisione a destra del nome del tuo calendario
* digitare il nome utente dell'utente Disroot con cui si desidera condividere il calendario
* premere enter

![](en/calendar_share_menu1.png)

Per condividere un calendario via email o link pubblico:

* premi il pulsante di condivisione a destra del nome del tuo calendario
* seleziona "Condividi link"
* riempire il campo dell'indirizzo e-mail con l'e-mail della persona con cui si desidera condividere il calendario
* premere enter
* per ottenere semplicemente il collegamento premere il simbolo della catena accanto al simbolo della busta della posta

![](en/calendar_share_menu2.png)


## Importare calendari
Se ha un file .ics con i tuoi calendari da importare, vai nell'applicazione calendario di Disroot, accedi a "Impostazioni & importa" nella parte bassa a sinistra dello schermo.

![](en/calendar_import_menu1.png)

E selezioni l'impostazione "Importa calendari".

![](en/calendar_import_menu2.png)
