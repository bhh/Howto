---
title: "Calendario: Móvil"
published: true
visible: false
updated:
        last_modified: "Julio 2019"
        app: Nextcloud
        app_version: 15
taxonomy:
    category:
        - docs
    tags:
        - calendario
        - nube
        - sincronización
page-toc:
    active: false
---

# Aplicaciones de Calendarios para móviles

**Disroot** tiene la aplicación Calendario habilitada.

Para configurar y utilizar calendarios de **Disroot** en tu dispositivo, revisa el siguiente tutorial:

### [Nextcloud: Clientes para Móvil](/tutorials/cloud/clients/mobile)
