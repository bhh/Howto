---
title: News
published: true
visible: false
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - apps
        - news
visible: true
page-toc:
    active: false
---

# News

A **RSS/Atom** feed reader which can be synced with many mobile devices.

---

## [Web interface](web)
- Manage your news feeds from the web

## [Desktop clients](desktop)
- Desktop clients and integration for reading, organizing and synchronizing your news feeds

## [Mobile clients](/tutorials/cloud/clients/mobile)
- Mobile clients for reading, organizing and synchronizing your news feeds
