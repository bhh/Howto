---
title: Android
visible: false
taxonomy:
    category:
        - docs
visible: true
page-toc:
     active: false
---

Ci-dessous vous pouvez apprendre comment intégrer nextcloud avec votre appareil Android.

- [Application Nextcloud](nextcloud-app)
- [Synchronisation des calendriers, contacts et tâches](calendars-contacts-and-tasks)
- [Synchronisation des actualités](using-news)
- [Synchronisation des notes](Using-notes)
![](android.jpg)
