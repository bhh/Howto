---
title: Cloud
subtitle: "Basics, settings, syncing, clients"
icon: fa-cloud
updated:
published: true
taxonomy:
    category:
        - docs
        - topic
    tags:
        - cloud
        - nextcloud
page-toc:
    active: false
---

![](nc_logo.png)

**Nextcloud** is a free and open source software that allows you to upload and storage files on a server (it could be a server of your own), and syncing them with different devices to which you can safely access from anywhere via internet. Some of the features it provides are: calendars, contacts and bookmarks synchronization, call/video conference, a basic project board, news feeder and more.<br>
It is **Disroot**'s core service and the main user interface we try to integrate with most of the apps we offer.

In the following chapters of this **Cloud** howto, we will learn about the interface, the basics of its operation and the main features of **Nextcloud**, as well as some basic actions on files and personal settings.
