---
title: 'Pads'
published: true
visible: true
taxonomy:
    category:
        - docs
page-toc:
  active: true
---

Etherpad et Ethercalc sont des applications collaboratives pour l'édition de documents en temps réel multi-utilisateurs. Vous pouvez les ouvrir directement à partir de votre navigateur: https://pad.disroot.org1 et https://calc.disroot.org1 Aucun compte utilisateur n'est nécessaire pour les utiliser. Cependant, notre cloud est livré avec un plugin très pratique qui vous aide à garder une trace de tous vos pads, comme s'ils étaient vos fichiers.

**Note!** Les" Pads" et "calcs" ne sont pas des fichiers contenant vos données mais des liens vers votre travail stockés sur https://pad.disroot.org ou https://calc.disroot.org.

# Idée derrière les pads
L'idée derrière ce logiciel est très simple. C'est un éditeur de texte/tableur qui vit sur le web. Tout ce que vous tapez est automatiquement écrit dans votre pad. Vous pouvez travailler sur un document avec plusieurs personnes en même temps sans avoir besoin d'enregistrer et de transmettre des copies des documents. Une fois votre travail terminé, vous pouvez exporter le pad au format de votre choix. Pour en savoir plus sur l'interface, vous pouvez consulter cette page (page en construction).

# Usage
Ci-dessous vous pouvez lire comment faire usage de ownpad - un plugin pour l'instance du cloud de disroot.

# Créer de nouveaux pads
La création d'un nouveau pad fonctionne de la même façon que la création d'un nouveau fichier. Cliquez sur l'icône "+" et choisissez de créer soit un pad (éditeur de texte) soit un calc (tableur). Une fois que vous lui aurez donné un nom, un nouveau fichier avec l'extension .pad/.calc sera créé. Vous pouvez déplacer, partager ce fichier comme n'importe quel autre fichier que vous avez sur votre compte cloud.

![](en/pads_add.gif)

# Ouverture d'un nouveau pad
Pour ouvrir le nouveau pad créé, cliquez simplement sur son nom. L'application ouvrira votre pad en utilisant pad.disroot.org/calc.disroot.org à l'intérieur de l'interface cloud. Vous pouvez maintenant facilement travailler sur votre document et une fois que vous avez terminé, utilisez l'icône rouge sur votre droite pour le fermer. Toutes les modifications sont écrites dès que vous commencez à taper, il n'est donc pas nécessaire de sauvegarder le fichier avant la saisie. Puisque le fichier vit dans le web, vous pouvez y revenir à tout moment.

![](en/pads_inapp_name.gif)

# Partager

Partager des pads avec d'autres utilisateurs est la même chose que de partager des fichiers normaux. Cliquez sur l'icône "Partage" et sélectionnez avec qui vous souhaitez partager le fichier. Note: utiliser le lien public ne fonctionnera pas aussi facilement puisque ce ne sont pas des fichiers normaux.

![](en/pads_inapp_name2.gif)

Afin de partager avec quelqu'un en dehors du cloud Disroot, vous devez partager le lien réel vers le pad. Vous pouvez le faire en ouvrant votre pad dans le navigateur et en survolant son nom. Vous verrez le lien réel vers le pad, que vous pourrez ensuite copier et envoyer à toute personne avec qui vous souhaitez partager le pad.

# Effacer les blocs-notes
Il est impossible de supprimer les pads. Une fois créés, ils restent en ligne. En fait n'importe qui capable de deviner l'url pourrait le chercher. C'est pourquoi le plugin de disroot cloud crée des liens vers vos pads en utilisant des chaînes aléatoires au lieu du nom que vous spécifiez lors de la création du fichier. De cette façon, les liens vers les pads sont impossibles à deviner, ce qui les rend inoffensifs pour vous et les utilisateurs avec qui vous partagez les pads.
