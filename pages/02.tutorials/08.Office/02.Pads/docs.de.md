---
title: 'Pads'
updated:
published: true
visible: true
indexed: true
taxonomy:
    category:
        - docs
    tags:
        - Pad
        - Etherpad
        - EtherCalc
page-toc:
  active: false
---

# Pads

## Was ist Pads?
![](/home/icons/etherpad.png)
**Etherpad** und **EtherCalc** sind hilfreiche Anwendungen zur Echtzeit-Dokumentenbearbeitung durch mehrere Benutzer gleichzeitig. Du kannst sie direkt über Deinen Browser erreichen unter: [https://pad.disroot.org](https://pad.disroot.org) und [https://calc.disroot.org](https://calc.disroot.org).<br>Du benötigst keinen Benutzeraccount, um **Etherpad** und **EtherCalc** zu benutzen. Dennoch beinhaltet unsere Cloud ein sehr nützliches Plugin, das Dir hilft, die Übersicht über all Deine Pads zu behalten. Genau so, als wären sie "klassische" Dateien.|

!! ![](/home/icons/note.png)
!! "Pads" und "Calcs" sind keine Dateien, die Deine Arbeit enthalten, sondern Links zu Deiner Arbeit, die in  https://pad.disroot.org oder https://calc.disroot.org gelagert wird.|

# Die Idee hinter Pads...
... ist sehr einfach. Es ist ein Text-/Tabellen-Editor, der im Netz lebt. Alles, was Du eingibst, wird automatisch in Dein Pad geschrieben. Du kannst mit vielen Leuten gleichzeitig an einem Dokument arbeiten, ohne das Dokument ständig speichern und Kopien an die Mitwirkenden verteilen zu müssen. Wenn Deine Arbeit erledigt ist, kannst Du das Pad in ein Dateiformat Deiner Wahl exportieren.

### [Etherpad](etherpad)
- Online-Editor zur gemeinschaftlichen Bearbeitung von Text-Dokumenten

### EtherCalc (Demnächst verfügbar)
- Online-Editor zur gemeinschaftlichen Bearbeitung von Tabellenkalkulationen

### [Padland](padland)
- Pad-Managementtool für Android
