---
title: 'Chat: Transports'
published: true
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - chat
        - xmpp
page-toc:
    active: true
---

# XMPP Transports

In XMPP realm, transports are the means to connect to different protocols.

Currently, Disroot Chat provides following transports:

1. [IRC](https://howto.disroot.org/en/tutorials/chat/transports/irc)
