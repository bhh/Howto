---
title: 'Forum'
subtitle: "Discourse basics"
icon: fa-stack-exchange
published: true
taxonomy:
    category:
        - docs
        - topic
    tags:
        - Forum
        - Discourse
page-toc:
    active: false
---

![](/home/icons/discourse.png)

**Discourse** is an Open Source internet forum and mailing list management software.
<br>
<br>
