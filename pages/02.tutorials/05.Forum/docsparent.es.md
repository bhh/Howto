---
title: 'Foro'
subtitle: "Guía básica de Discourse"
icon: fa-stack-exchange
published: true
taxonomy:
    category:
        - docs
        - topic
    tags:
        - Foro
        - Discourse
page-toc:
    active: false
---

![](/home/icons/discourse.png)

**Discourse** es un software de gestión de foros en línea y listas de correos de Código Abierto.
<br>
<br>
